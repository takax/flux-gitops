# flux-gitops

## Install Flux onto your cluster


```shell
flux bootstrap gitlab \
--owner=takax \
--repository=flux-gitops \
--branch=main \
--path=clusters/my-cluster \
--deploy-token-auth
```

For more detail, see [complete-a-bootstrap-installation](https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_tutorial.html#complete-a-bootstrap-installation).

### Install Agentk

- https://docs.gitlab.com/ee/user/clusters/agent/gitops/flux_tutorial.html#install-agentk

Agentk pods are defined in `clusters/my-cluster` directory. 
Check if pods are running with `kubectl -n gitlab get pods`.

